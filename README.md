Для запуска используйте команду выше.

```bash
docker-compose up
```

Документация:
* [nginx](http://nginx.org/ru/docs/)
* [Docker Compose](https://docs.docker.com/compose)
